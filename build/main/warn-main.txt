
This file lists modules PyInstaller was not able to find. This does not
necessarily mean this module is required for running you program. Python and
Python 3rd-party packages include a lot of conditional or optional modules. For
example the module 'ntpath' only exists on Windows, whereas the module
'posixpath' only exists on Posix systems.

Types if import:
* top-level: imported at the top-level - look at these first
* conditional: imported within an if-statement
* delayed: imported from within a function
* optional: imported within a try-except-statement

IMPORTANT: Do NOT post this list to the issue-tracker. Use it as a basis for
           yourself tracking down the missing module. Thanks!

missing module named 'org.python' - imported by copy (optional), setuptools.sandbox (conditional), xml.sax (delayed, conditional)
missing module named _posixsubprocess - imported by subprocess (conditional), multiprocessing.util (delayed)
missing module named pwd - imported by posixpath (delayed, conditional), shutil (optional), tarfile (optional), http.server (delayed, optional), webbrowser (delayed), pathlib (delayed, conditional, optional), netrc (delayed, conditional), getpass (delayed), distutils.util (delayed, conditional), distutils.archive_util (optional)
missing module named posix - imported by os (conditional, optional)
missing module named resource - imported by posix (top-level)
missing module named 'win32com.gen_py' - imported by win32com (conditional, optional), C:\Users\Devaditya\AppData\Roaming\Python\Python37\site-packages\PyInstaller\hooks\rthooks\pyi_rth_win32comgenpy.py (top-level)
missing module named _frozen_importlib_external - imported by importlib._bootstrap (delayed), importlib (optional), importlib.abc (optional)
excluded module named _frozen_importlib - imported by importlib (optional), importlib.abc (optional), PyInstaller.loader.pyimod02_archive (delayed)
missing module named urllib.pathname2url - imported by urllib (conditional), PyInstaller.lib.modulegraph._compat (conditional)
missing module named urllib.getproxies_environment - imported by urllib (conditional), requests.compat (conditional)
missing module named urllib.proxy_bypass_environment - imported by urllib (conditional), requests.compat (conditional)
missing module named urllib.proxy_bypass - imported by urllib (conditional), requests.compat (conditional)
missing module named urllib.getproxies - imported by urllib (conditional), requests.compat (conditional)
missing module named urllib.urlencode - imported by urllib (conditional), requests.compat (conditional)
missing module named urllib.unquote_plus - imported by urllib (conditional), requests.compat (conditional)
missing module named urllib.quote_plus - imported by urllib (conditional), requests.compat (conditional)
missing module named urllib.unquote - imported by urllib (conditional), requests.compat (conditional)
missing module named urllib.quote - imported by urllib (conditional), requests.compat (conditional)
missing module named termios - imported by tty (top-level), getpass (optional)
missing module named vms_lib - imported by platform (delayed, conditional, optional)
missing module named 'java.lang' - imported by platform (delayed, optional), xml.sax._exceptions (conditional)
missing module named java - imported by platform (delayed)
missing module named _scproxy - imported by urllib.request (conditional)
missing module named _winreg - imported by platform (delayed, optional), requests.utils (delayed, conditional, optional), pkg_resources._vendor.appdirs (delayed, conditional), numpy.distutils.cpuinfo (delayed, conditional, optional)
missing module named readline - imported by cmd (delayed, conditional, optional), code (delayed, conditional, optional), pdb (delayed, optional)
missing module named org - imported by pickle (optional)
missing module named grp - imported by shutil (optional), tarfile (optional), pathlib (delayed), distutils.archive_util (optional)
missing module named multiprocessing.get_context - imported by multiprocessing (top-level), multiprocessing.pool (top-level), multiprocessing.managers (top-level), multiprocessing.sharedctypes (top-level)
missing module named multiprocessing.TimeoutError - imported by multiprocessing (top-level), multiprocessing.pool (top-level)
missing module named multiprocessing.BufferTooShort - imported by multiprocessing (top-level), multiprocessing.connection (top-level)
missing module named multiprocessing.AuthenticationError - imported by multiprocessing (top-level), multiprocessing.connection (top-level)
missing module named multiprocessing.set_start_method - imported by multiprocessing (top-level), multiprocessing.spawn (top-level)
missing module named multiprocessing.get_start_method - imported by multiprocessing (top-level), multiprocessing.spawn (top-level)
missing module named pyimod03_importers - imported by C:\Users\Devaditya\AppData\Roaming\Python\Python37\site-packages\PyInstaller\hooks\rthooks\pyi_rth_pkgres.py (top-level)
missing module named _manylinux - imported by pkg_resources._vendor.packaging.tags (delayed, optional), setuptools._vendor.packaging.tags (delayed, optional)
missing module named 'pkg_resources.extern.pyparsing' - imported by pkg_resources._vendor.packaging.markers (top-level), pkg_resources._vendor.packaging.requirements (top-level)
missing module named 'com.sun' - imported by pkg_resources._vendor.appdirs (delayed, conditional, optional)
missing module named com - imported by pkg_resources._vendor.appdirs (delayed)
missing module named _uuid - imported by uuid (optional)
missing module named __builtin__ - imported by pkg_resources._vendor.pyparsing (conditional), numpy.core.numerictypes (conditional), numpy.core.numeric (conditional), numpy.lib.function_base (conditional), numpy.lib._iotools (conditional), numpy.ma.core (conditional), scipy._lib.six (conditional), numpy.distutils.misc_util (delayed, conditional), numpy (conditional), setuptools._vendor.pyparsing (conditional)
missing module named ordereddict - imported by pkg_resources._vendor.pyparsing (optional), setuptools._vendor.pyparsing (optional)
missing module named pkg_resources.extern.packaging - imported by pkg_resources.extern (top-level), pkg_resources (top-level)
missing module named pkg_resources.extern.appdirs - imported by pkg_resources.extern (top-level), pkg_resources (top-level)
missing module named StringIO - imported by urllib3.packages.six (conditional), six (conditional), requests.compat (conditional), PyInstaller.lib.modulegraph._compat (conditional), PyInstaller.lib.modulegraph.zipio (conditional), numpy.testing._private.utils (conditional), numpy.lib.utils (delayed, conditional), numpy.lib.format (delayed, conditional), scipy._lib.six (conditional)
missing module named Cookie - imported by requests.compat (conditional)
missing module named cookielib - imported by requests.compat (conditional)
missing module named urllib2 - imported by requests.compat (conditional), numpy.lib._datasource (delayed, conditional)
missing module named urlparse - imported by requests.compat (conditional), numpy.lib._datasource (delayed, conditional)
missing module named simplejson - imported by requests.compat (optional)
missing module named backports - imported by urllib3.packages.ssl_match_hostname (optional)
missing module named Queue - imported by urllib3.util.queue (conditional)
missing module named brotli - imported by urllib3.util.request (optional), urllib3.response (optional)
missing module named "'urllib3.packages.six.moves.urllib'.parse" - imported by urllib3.request (top-level), urllib3.poolmanager (top-level)
runtime module named urllib3.packages.six.moves - imported by http.client (top-level), urllib3.connectionpool (top-level), urllib3.util.response (top-level), 'urllib3.packages.six.moves.urllib' (top-level), urllib3.response (top-level), urllib3.util.queue (top-level)
missing module named socks - imported by urllib3.contrib.socks (optional)
missing module named _dummy_threading - imported by dummy_threading (optional)
missing module named 'typing.io' - imported by importlib.resources (top-level)
runtime module named six.moves - imported by cryptography.hazmat.backends.openssl.backend (top-level), cryptography.x509.general_name (top-level)
missing module named six.moves.range - imported by six.moves (top-level), cryptography.hazmat.backends.openssl.backend (top-level)
missing module named cryptography.x509.UnsupportedExtension - imported by cryptography.x509 (optional), urllib3.contrib.pyopenssl (optional)
